plugins {
    java
    application
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

tasks.compileJava {
    options.compilerArgs.add("--enable-preview")
}

tasks.withType(JavaExec::class) {
    jvmArgs?.add("--enable-preview")
}

application {
    mainClass.set("jdkserver.Main")
}

repositories {
    mavenCentral()
}

dependencies {
}