package jdkserver;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Main {

    public static void main(String... args) throws IOException {
        var concurrency = Concurrency.valueOf(System.getenv().getOrDefault("concurrency", "LOOM"));

        var server = HttpServer.create(new InetSocketAddress(8080), 0);
        server.setExecutor(concurrency.executor.get());
        server.createContext("/hello", Main::sayHello);
        System.out.println("App started, hit it on http://localhost:8080/hello");
        server.start();
    }

    static AtomicInteger reqCounter = new AtomicInteger(0);
    static void sayHello(HttpExchange ex) {
        try {
            System.out.printf("req#%d on %s%n", reqCounter.incrementAndGet(), Thread.currentThread());
            Thread.sleep(new Random().nextInt(15_000, 20_000)); // simulate heavy IO
            var message = "hello from jdkserver, using %s thread".formatted(Thread.currentThread().isVirtual() ? "virtual" : "platform");
            ex.sendResponseHeaders(200, message.length());
            var out = ex.getResponseBody();
            out.write(message.getBytes(UTF_8));
            out.flush();
            out.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

/** @noinspection unused*/
enum Concurrency {
    PLATFORM(() -> Executors.newFixedThreadPool(Integer.parseInt(System.getenv("thread_pool_size")))),
    LOOM(Executors::newVirtualThreadPerTaskExecutor);

    final Supplier<Executor> executor;

    Concurrency(Supplier<Executor> executor) {
        this.executor = executor;
    }
}

