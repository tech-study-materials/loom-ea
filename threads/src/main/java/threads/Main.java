package threads;

import java.util.concurrent.CountDownLatch;
import java.util.stream.IntStream;

public class Main {

    public static void main(String... args) {
        spawnThreads(Thread.ofVirtual());
//        spawnThreads(Thread.ofPlatform());
        await(); //needed for virtual threads, which are all daemons (so JVM would exit before finishing them - too fast to check/demo memory usage
    }

    static void spawnThreads(Thread.Builder threadBuilder) {
        IntStream
                .rangeClosed(1, 100_000)
                .forEach(i -> threadBuilder.start(task(i)));
    }

    static Runnable task(int threadNumber) {
        return () -> {
            var threadType = Thread.currentThread().isVirtual() ? "virtual" : "platform";
            if(threadNumber % 1000 == 0)
                System.out.printf("started %s thread #%d : %s%n", threadType, threadNumber, Thread.currentThread());
            await();
        };
    }

    private static void await() {
        try {
            new CountDownLatch(1).await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


