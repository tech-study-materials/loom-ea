plugins {
    java
    application
}

tasks.compileJava {
    options.compilerArgs.add("--enable-preview")
}

tasks.withType(JavaExec::class) {
    jvmArgs?.add("--enable-preview")
}

application {
    mainClass.set("threads.Main")
}

repositories {
    mavenCentral()
}

dependencies {
}
