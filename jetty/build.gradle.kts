plugins {
    java
    application
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

tasks.compileJava {
    options.compilerArgs.add("--enable-preview")
}

tasks.withType(JavaExec::class) {
    jvmArgs?.add("--enable-preview")
}

application {
    mainClass.set("jetty.Main")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.eclipse.jetty:jetty-server:11.0.11")
    implementation("org.eclipse.jetty:jetty-servlet:11.0.11")
    implementation("org.slf4j:slf4j-simple:2.0.0-alpha6")
}
