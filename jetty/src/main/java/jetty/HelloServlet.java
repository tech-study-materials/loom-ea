package jetty;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class HelloServlet extends HttpServlet {
    AtomicInteger reqCounter = new AtomicInteger(0);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.printf("req#%d on %s%n", reqCounter.incrementAndGet(), Thread.currentThread());
        try {
            Thread.sleep(new Random().nextInt(15_000, 20_000)); // simulate heavy IO
        } catch (InterruptedException e) {
            throw new ServletException(e);
        }
        resp.setStatus(200);
        var message = "hello from jetty, using %s thread".formatted(Thread.currentThread().isVirtual() ? "virtual" : "platform");
        resp.getWriter().println(message);
    }
}
