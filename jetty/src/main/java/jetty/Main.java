package jetty;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ThreadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class Main {

    public static void main(String... args) throws Exception {
        var concurrency = Concurrency.valueOf(System.getenv().getOrDefault("concurrency", "LOOM"));

        var server = new Server(concurrency.threadpool.get());
        var connector8080 = new ServerConnector(server);
        connector8080.setPort(8080);
        server.setConnectors(new Connector[]{connector8080});
        var servlet = new ServletHandler();
        servlet.addServletWithMapping(HelloServlet.class, "/hello");
        server.setHandler(servlet);
        server.start();
        System.out.println("App started, hit it on http://localhost:8080/hello");
        server.join();
    }
}

/** @noinspection unused*/
enum Concurrency {
    PLATFORM(() -> new QueuedThreadPool(Integer.parseInt(System.getenv("thread_pool_size")))),
    LOOM(VirtualThreadPool::new);

    final Supplier<ThreadPool> threadpool;

    Concurrency(Supplier<ThreadPool> threadpool) {
        this.threadpool = threadpool;
    }
}

class VirtualThreadPool implements ThreadPool {
    ExecutorService executorService = Executors.newVirtualThreadPerTaskExecutor();

    @Override
    public void join() throws InterruptedException {
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
    }

    @Override
    public int getThreads() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int getIdleThreads() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isLowOnThreads() {
        return false;
    }

    @Override
    public void execute(Runnable command) {
        executorService.submit(command);
    }
}


