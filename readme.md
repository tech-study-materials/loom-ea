# Configure build

* edit `gradle.properties` to point to your jdk20 directory

# Play with threads

* `gradlew threads:clean threads:run`
* observe memory consumption of the JVM process
  * on Win10, you should see the process eat up ~5.5 GB of RAM, might take ~5 minutes to start all threads
  * on Mac, app should crash after starting ~8k threads 
* edit threads/Main.java, switch platform threads to virtual
* `gradlew threads:clean threads:run`
* observe memory consumption of the JVM process
  * should start all threads within seconds, eating few hundred MB of RAM 

# Build servers 
 
* build jdkserver image:
  * `gradlew jdkserver:clean jdkserver:shadowJar`
  * `cd jdkserver && docker build -t loom-jdkserver .`
* build jetty image:
  * `gradlew jetty:clean jetty:shadowJar`
  * `cd jetty && docker build -t loom-jetty .`

# Run servers

* `docker compose up`
  * jdkserver with platform threads -> [localhost:8081/hello](http://localhost:8081/hello)
  * jdkserver with virtual threads -> [localhost:8082/hello](http://localhost:8082/hello)
  * jetty with platform threads -> [localhost:8083/hello](http://localhost:8083/hello)
  * jetty with virtual threads -> [localhost:8084/hello](http://localhost:8084/hello)

# Load test

* `gradlew loadtest:gatlingRun`
