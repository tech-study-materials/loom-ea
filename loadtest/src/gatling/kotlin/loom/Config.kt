package loom


val userCount = 1_000
val host = "localhost"
//val host = "home-pc"

enum class Config(val port: Int) {
    DEV(8080),
    JDKSERVER_PLATFORM(8081),
    JDKSERVER_VIRTUAL(8082),
    JETTY_PLATFORM(8083),
    JETTY_VIRTUAL(8084);

    fun url() = "http://$host:$port"
    fun description() = "$name, $userCount users"
}