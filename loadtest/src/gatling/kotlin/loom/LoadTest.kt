package loom

import io.gatling.javaapi.core.CoreDsl.*
import io.gatling.javaapi.core.Simulation
import io.gatling.javaapi.http.HttpDsl.http
import java.time.Duration

abstract class LoadTest(underTest: Config) : Simulation() {
    val target = http
        .baseUrl(underTest.url())

    val users = rampUsers(userCount)
        .during(10)

    val getHelloAction = exec(
        http("hello")
            .get("/hello")
            .requestTimeout(Duration.ofSeconds(30))
    )

    val scenario = scenario(underTest.description())
        .exec(getHelloAction)
        .injectOpen(users)
        .protocols(target)

    init {
        setUp(scenario)
    }
}