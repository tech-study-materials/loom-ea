plugins {
    kotlin("jvm") version "1.6.10"
    id("io.gatling.gradle") version "3.8.2"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
}